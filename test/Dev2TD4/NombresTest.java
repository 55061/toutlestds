/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dev2TD4;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Vincs
 */
public class NombresTest {

    @Test
    public void testSum_tousStrictementPositifs() {
        Nombres instance = new Nombres();
        instance.add(4);
        instance.add(12);
        instance.add(52);
        Integer expResult = 68;
        Integer result = instance.sum();
        assertEquals(expResult, result);
    }

}
