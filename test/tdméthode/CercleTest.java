/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tdméthode;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Vincs
 */
public class CercleTest {

    public CercleTest() {
    }

    @Test
    public void testPérimètre() {
        System.out.println("PerimetreTo0");
        double rayon = 0.0;
        double expResult = 0.0;
        double result = Cercle.périmètre(rayon);
        assertEquals(expResult, result, 0.0);

    }

    @Test
    public void testPérimètre2() {
        System.out.println("PerimetrePositif");
        double rayon = 4.0;
        double expResult = 25.13;
        double result = Cercle.périmètre(rayon);
        assertEquals(expResult, result, 0.01);

    }

    @Test(expected = IllegalArgumentException.class)
    public void périmètre_rayonNégatif_IAException() {
        System.out.println("PerimetreNegatif");
        Cercle.périmètre(-5);
    }
}
