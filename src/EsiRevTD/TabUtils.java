package EsiRevTD;

public class TabUtils {

    static void afficherTab(double[] tab) {
        for (double element : tab) {
            System.out.print(element + " ");
        }
        System.out.println();
    }

    static void afficherTabSans0(double[] tab) {
        for (double element : tab) {
            if (element == 0) {
            } else {
                System.out.print(element + " ");
            }
        }
        System.out.println();
    }

    //qui retourne le minimum du tableau passé en paramètre ;
    static double min(double[] tab) {
        double min = tab[0];

        for (int i = 1; i < tab.length - 1; i++) {
            if (tab[i] < min) {
                min = tab[i];
            }
        }
        return min;
    }

    //qui retourne le maximum du tableau ;
    static double max(double[] tab) {
        double max = tab[0];

        for (int i = 1; i <= tab.length - 1; i++) {
            if (max < tab[i]) {
                max = tab[i];
            }
        }
        return max;
    }

    //qui retourne la somme des éléments du tableau ; 
    static double somme(double[] tab) {
        double somme = 0;
        for (double valeur : tab) {
            somme = somme + valeur;
        }
        return somme;
    }

    //qui retourne la moyenne des éléments du tableau ;
    static double moyenne(double[] tab) {
        double somme = 0;
        for (double valeur : tab) {
            somme = somme + valeur;
        }
        double moyenne;
        moyenne = somme / tab.length;

        return moyenne;
    }

    //qui retourne une copie du tableau passé en paramètre ;
    static double[] copie(double[] tab) {
        double[] copie = new double[tab.length];
        for (int i = 0; i < tab.length; i++) {
            copie[i] = tab[i];
        }
        return copie;
    }
// qui retourne une copie en miroir du tableau passé en paramètre

    static double[] miroir(double[] tab) {
        double[] miroir = new double[tab.length];
        int i = 0;

        for (double valeur : tab) {
            miroir[miroir.length - 1 - i] = valeur;
            i++;
        }
        return miroir;
    }

    //retourne l’indice du maximum du tableau passé en paramètre ;
    static int indiceMax(double[] tab) {
        double max = tab[0];
        int indexMax = 0;

        for (int i = 0; i <= tab.length - 1; i++) {
            if (max < tab[i]) {
                max = tab[i];
                indexMax = i;
            }
        }
        return indexMax;
    }

    //retourne vrai si le tableau est trié par ordre croissant et faux sinon ;
    static boolean estTrié(double[] tab) {
        boolean trié = true;
        for (int i = 0; i <= tab.length - 1; i++) {
            for (int j = tab.length - 1; j >= 0; j--) {
                if (tab[j] < tab[i]) {
                    trié = false;
                } else if (j <= i) {
                    break;
                }
            }
        }
        return trié;
    }

    //retourne l’indice de cette valeur dans le tableau.
    static int indice(double[] tab, int valeur) {
        boolean check = false;
        int index = 0;
        for (double val : tab) {
            if (val == valeur) {
                check = true;
            }
        }
        if (!check) {
            throw new IllegalArgumentException();
        } else {
            for (int i = 0; i <= tab.length - 1; i++) {
                if (tab[i] == valeur) {
                    index = i;
                }

            }
        }
        return index;
    }

    //retourne vrai si un tableau de String contient un mot passé en paramètre 
    static boolean contient(String[] tab, String mot) {
        boolean check = false;
        for (String valeur : tab) {
            if (valeur.equals(mot)) {
                check = true;
                break;
            }
        }
        return check;
    }

    //retourne vrai si un élément du tableau est proche de la valeur à l’erreur près
    static boolean proche(double[] tab, double valeur, double erreur) {
        boolean check = false;
        for (double val : tab) {
            if (valeur <= (val + erreur) && valeur >= (val - erreur)) {
                check = true;
            }
        }
        return check;
    }

    //retourne vrai si le tableau possède des doublons (2 fois la même valeur) et faux sinon
    static boolean doublons(double[] tab) {
        boolean check = false;
        for (int i = 0; i <= tab.length - 1; i++) {
            for (int j = i + 1; j <= tab.length - 1; j++) {
                if (tab[i] == tab[j]) {
                    check = true;
                    break;
                }
            }
        }
        return check;
    }

    //retourne le nombre d’éléments négatifs du tableau
    static int nbNégatifs(double[] tab) {
        int compt = 0;
        for (double valeur : tab) {
            if (valeur < 0) {
                compt++;
            }
        }
        return compt;
    }

    //échange la valeur se trouvant à l’indice indice1 avec la valeur se trouvant à l’indice indice2
    static void echange(double[] tab, int indice1, int indice2) {
        double x = 1;
        double y = 1;
        for (int i = 0; i < tab.length; i++) {
            if (i == indice1) {
                x = tab[i];
            } else if (i == indice2) {
                y = tab[i];
            }

        }
        tab[indice1] = y;
        tab[indice2] = x;

    }

    //retourne un tableau contenant les indices des minimums
    static double[] indicesMin(double[] tab) {
        double[] indexMin = new double[tab.length];
        int index=0;
        double min = tab[0];
        return indexMin;
    }

    public static void main(String[] args) {
        double[] tableau = {1, 2, 1, 3, 5};
        String[] liste = {"un", "deux", "trois", "quatre", "cinq"};
        System.out.println("minimum: " + min(tableau));
        System.out.println("maximum: " + max(tableau));
        System.out.println("sommes: " + somme(tableau));
        System.out.println("moyenne: " + moyenne(tableau));
        System.out.print("copie: ");
        afficherTab(copie(tableau));
        System.out.println();
        System.out.print("miroir: ");
        afficherTab(miroir(tableau));
        System.out.println("indice max: " + indiceMax(tableau));
        System.out.println("croissant ou non ?: " + estTrié(tableau));
        System.out.println("l'indice de la valeur choisie est: " + indice(tableau, 3));
         
        System.out.println("Contient le mot en parametre ?: " + contient(liste, "deux"));
        System.out.println("Proche de la valeur d'erreur?: " + proche(tableau, 6, 1));
        System.out.println("doublons ?: " + doublons(tableau));
        System.out.println("Nombre de négatifs: " + nbNégatifs(tableau));

        System.out.println("indices des minimums: ");
        afficherTabSans0(indicesMin(tableau));
        System.out.println("échange: ");
        echange(tableau, 3, 0);
        afficherTab(tableau);
    }
}
