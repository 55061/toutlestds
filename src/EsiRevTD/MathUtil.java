package EsiRevTD;

import java.util.Scanner;

public class MathUtil {

    // retourne le minimum entre x et y
    static double min(double x, double y) {
        double min2 = x;
        if (x > y) {
            min2 = y;
        }
        return min2;
    }

    static double min(double x, double y, double z) {
        double min2 = x;
        if (x > y) {
            min2 = y;
        }
        if (z < x) {
            min2 = z;
        }
        return min2;
    }

    public static void main(String[] args) {
        Scanner clavier = new Scanner(System.in);
        
        System.out.println("Valeur de x");
        double x = clavier.nextInt();
        
        System.out.println("Valeur de y");
        double y = clavier.nextInt();
        
        System.out.println("Valeur de y");
        double z = clavier.nextInt();
        
        System.out.println(min(x, y));
        System.out.println(min(x, y, z));
    }
}
