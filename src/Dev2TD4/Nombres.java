package Dev2TD4;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Nombres {

    private List<Integer> nombres;

    public Nombres() {
        this.nombres = new ArrayList<>();
    }

    public Nombres(List<Integer> nombres) {
        this.nombres = new ArrayList<>();
    }

    void remove(Integer val) {
        this.nombres.remove(val);
    }

    void add(Integer val) {
        this.nombres.add(val);
    }

    Integer sum() {

        return TdListes.sum(this.nombres);

    }

    void concatenation(List<Integer> e) {
        e.forEach((Integer variables) -> {
            this.add(variables);
        });
    }

    void afficher() {
        nombres.forEach((Integer variable) -> {
            System.out.print(variable + " ");
        });
        System.out.println("");
    }

    void enleveDoublon() {
        int i = 0;
        do {
            if (Objects.equals(nombres.get(i), nombres.get(i + 1))) {
                nombres.remove(i);
            } else {
                i++;
            }
        } while (i < nombres.size() - 1);
    }

}
