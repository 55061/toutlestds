package Dev2TD4;

import Dev2TD2.Moment;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TdListes {

    static void afficherList(List<Integer> list) {
        for (int i = 0; i < list.size(); i++) {
            System.out.print(list.get(i) + " ");
        }
        System.out.println("");
    }

    static int sum(List<Integer> list) {
        int sum = 0;
        for (Integer valeur : list) {
            sum = sum + valeur;
        }
        return sum;
    }

    static void enleveDoublon(List<Integer> list) {
        int i = 0;

        do {
            if (Objects.equals(list.get(i), list.get(i + 1))) {
                list.remove(i);
            } else {
                i++;
            }
        } while (i < list.size() - 1);

    }

    public static void main(String[] args) {
        Moment moment = new Moment();
        Moment moment1 = new Moment(16, 56, 20);
        Nombres nombres = new Nombres();

        List<Integer> list = new ArrayList<>();
        List<Moment> moments = new ArrayList<>();

        moments.add(moment);
        moments.add(moment1);
        list.add(54);
        list.add(54);
        list.add(42);
        list.add(42);
        list.add(42);
        list.add(10);
        list.add(10);
        list.add(2);
        list.add(2);

        //afficherList(list);
        // list.add(0, 44);
        //afficherList(list);
        //list.add(1, 43);
        //list.remove(list.size() - 1);
        //afficherList(list);
        //list.remove(Integer.getInteger("43"));
        //afficherList(list);
        //System.out.println(sum(list));
        nombres.add(34);
        nombres.add(45);
        nombres.add(2);
        nombres.add(3784);

        nombres.concatenation(list);

        nombres.afficher();
        afficherList(list);
        enleveDoublon(list);
        afficherList(list);
        nombres.afficher();
        nombres.enleveDoublon();
        nombres.afficher();

        int nbLignes = 3;
        int nbCols = 5;
        boolean stop = false;
        int lg = 0, col = 0;
        while (!stop) {
            System.out.printf("[%d %d]\n", lg, col);
            stop = !(++col < nbCols) && !(++lg < nbLignes);
            col %= nbCols;
        }

    }
}
