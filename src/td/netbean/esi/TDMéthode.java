/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package td.netbean.esi;

/**
 *
 * @author Vincs
 */
public class TDMéthode {

    public static void main(String[] args) {
        System.out.println("Le périmètre d’un cercle de rayon 10 est: " + périmètre(10));
    }

    static double périmètre(double rayon) {
        return 2 * Math.PI * rayon;

    }

    //reçoit un rayon en paramètre et retourne l’aire du cercle.
    double aireCercle(double rayon) {
        return Math.PI * rayon * rayon;
    }
}
