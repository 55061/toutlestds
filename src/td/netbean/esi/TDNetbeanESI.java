package td.netbean.esi;

import java.util.Scanner;

public class TDNetbeanESI {

    static void unAn(int n) {
        int i = 1;
        while (i <= n) {
            System.out.print(i);
            i++;
        }
        System.out.println("");
    }

    static void unAnPair(int n) {

        for (int i = 1; i <= n; i++) {
            if (i % 2 == 0) {
                System.out.print(i);
            }
        }
        System.out.println("");

    }

    static void moinsNaN(int n) {
        for (int i = -n; i <= n; i++) {
            System.out.print(i);
        }
    }

    public static void main(String[] args) {
        Scanner clavier = new Scanner(System.in);
        int n = clavier.nextInt();
        unAn(n); // la méthode de 1 à n
        unAnPair(n); // la méthode de 1 à n seulement pair 
        moinsNaN(n); // la méthode de -n à n
    }
}
