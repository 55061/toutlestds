package td.netbean.esi;

import java.util.Scanner;

public class TDBoucleFor {

    static void boucleUtils() {
        Scanner clavier = new Scanner(System.in);
        String texte = clavier.nextLine(); //lit du texte au clavier

        System.out.println(texte);
        System.out.println("Le texte entré est: " + texte);

        int longueur = texte.length(); //le nombre de caractères
        System.out.println("La longueur du texte est: " + longueur);

        char premièreLettre = texte.charAt(0);
        char dernièreLettre = texte.charAt(longueur - 1);
        System.out.println(premièreLettre);
        System.out.println(dernièreLettre);

        for (int i = 0; i < texte.length(); i = i + 1) { // parcours la chaine de caractères
            System.out.print(texte.charAt(i));
        }

        System.out.println(); //passe à la ligne

        for (int i = texte.length() - 1; i >= 0; i = i - 1) { // parcours la chaine de caractères à l’envers
            System.out.print(texte.charAt(i));
        }
    }

    static void voyelle() {
        Scanner clavier = new Scanner(System.in);
        System.out.println("Entrez un mot: ");
        String mot = clavier.nextLine();
        char[] voyelles = {'a', 'e', 'i', 'o', 'u', 'y'};
        for (int i = 0; i <= voyelles.length; i++) {
            if (voyelles[i] == mot.charAt(0)) {
                System.out.println(mot.charAt(0) + " est une voyelle");
                break;
            } else {
                System.out.println(mot.charAt(0) + " est une consonne");
                break;
            }

        }

    }

    static void voyellesSansTab() {
        Scanner clavier = new Scanner(System.in);
        System.out.println("Entrez un mot: ");
        String mot = clavier.nextLine();
        if ('a' == mot.charAt(0)
                || 'e' == mot.charAt(0)
                || 'i' == mot.charAt(0)
                || 'o' == mot.charAt(0)
                || 'u' == mot.charAt(0)
                || 'i' == mot.charAt(0)) {
            System.out.println(mot.charAt(0) + " est une voyelle");

        } else {
            System.out.println(mot.charAt(0) + " est une consonne");
        }

    }

    public static void main(String[] args) {

        //boucleUtils(); //quelques outils pour les boucles
        //voyelle();//demande à l’utilisateur un mot et affiche si la première lettre est une voyelle ou non.
        voyellesSansTab();
    }
}
