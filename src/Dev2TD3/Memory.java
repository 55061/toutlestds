package Dev2TD3;

import java.util.Scanner;

public class Memory {


    private  static  Game game;
    /*
    demande à l’utilisateur avec combien de paires il veut jouer et crée le jeu correspondant. La partie est lancée et
    quand une partie est terminée, la méthode affiche le nombre de tours que le joueur
    */
    public static void main(String[] args) {
        Scanner clavier = new Scanner(System.in);
        System.out.println("Avec combien de cartes voulez vous jouez ?");
        int nbPaires = clavier.nextInt();
        Memory.game = new Game(nbPaires);
        

    }

    

    public static int askPos1() {
        Scanner clavier = new Scanner(System.in);
        System.out.println("Choisissez une 1ere carte: ");
        int pos1 = clavier.nextInt();
        while (!game.isHidden(pos1)) {
            System.out.println("Cette carte est déja choisie, choisis en une autre");
            pos1 = clavier.nextInt();
        }
        System.out.println("C'est un " + game.getCardValue(pos1));

        return pos1;
    } //demande à l’utilisateur la position de la 1ere carte, affiche la valeur de cette carte et retourne la position entrée par l’utilisateur

    public static  int askPos2() {
        Scanner clavier = new Scanner(System.in);
        System.out.println("Choisissez une 2eme carte: ");
        int pos2 = clavier.nextInt();
        while (!game.isHidden(pos2)) {
            System.out.println("Cette carte est déja choisie, choisis en une autre");
            pos2 = clavier.nextInt();
        }
        System.out.println("C'est un " + game.getCardValue(pos2));
        return pos2;
    }}
//demande à l’utilisateur la position de la 2eme carte, affiche la valeur de cette carte et retourne la position entrée par l’utilisateur

   
