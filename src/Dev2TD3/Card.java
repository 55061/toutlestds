package Dev2TD3;

public class Card {

    private static int value; // valeur de la carte
    private static boolean hidden = true; // état de la carte

    public static int getValue() {
        return value;
    } // retourne la valeur de la carte 

    public static boolean isHidden() {
        return hidden;
    } // retourne l'état de la carte

    public static void reveal(boolean hidden) {
        Card.hidden = hidden;

    } // revèle la carte

    public Card(int value) {
        if (value <= 0) {
            throw new IllegalArgumentException();
        }
        Card.value = value;
    } // constructeur

    @Override
    public String toString() {
        if (Card.hidden == true) {
            return "?";
        } else {
            return "" + getValue();
        }
    } // retourne la valeur ou "?" selon l'état de la carte

    @Override
    public boolean equals(Object obj) {
        boolean equals;
        if (getClass() != obj.getClass()) {
            return equals = false;
        } else {
            return equals = true;
        }

    } // permet de comparer des objets entre-eux.

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + Card.value;
        hash = 37 * hash + (Card.hidden ? 1 : 0);
        return hash;
    } // hashCode
}
