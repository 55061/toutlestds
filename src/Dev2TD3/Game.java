package Dev2TD3;

import java.util.Random;

public class Game {

    private Card[] cards;

    public Game(int n) {
        int i = 1; // value
        int j = 0; // index
        if (n > 20 && n < 2) {
            throw new IllegalArgumentException();
        }
        this.cards = new Card[n * 2];

        while (i <= n) {
            Card card = new Card(i);
            cards[j] = card;
            cards[j + 1] = card;
            j = j + 2;
            i++;

        }

    } // recoit en parametre le nombre de paires et initialise le tableau de cartes

    public void shuffle() {
        int index;
        Random random = new Random();
        for (int i = cards.length - 1; i > 0; i--) {
            index = random.nextInt(i + 1);
            swap(cards, i, index);
        }
    } // mélange les cartes.

    public int getSize() {

        return cards.length;
    } //retourne le nombre de cartes dans le jeu

    public boolean isGameOver() {
        boolean gameOver = true;
        for (Card carte : cards) {
            if (carte.isHidden()) {
                gameOver = false;
            }
        }
        return gameOver;
    } //vérifie si le jeu est terminé, elle retourne vrai si toutes les cartes sont révélées 

    public boolean checkPositions(int pos1, int pos2) {
        if (pos1 == pos2) {
            throw new IllegalArgumentException();
        }
        cards[pos1].reveal(false);
        cards[pos2].reveal(false);
        return (getCardValue(pos1) == getCardValue(pos2));
    } // check la pos des cartes et retroune si ce sont les memes ou pas

    public boolean isHidden(int pos) {

        return cards[pos].isHidden();
    } // retourne vrai si la carte en position pos est cachée 

    public int getCardValue(int pos) {

        return cards[pos].getValue();
    } // retourne la valeur de la carte en position pos
    public static void swap(Card[] array, int pos1, int pos2) {
        if (pos1 < 0 || pos2 < 0) {
            throw new IllegalArgumentException("Les positions doivent être des "
                    + "entiers positifs " + pos1 + " " + pos2);
        }
        
    }
}
