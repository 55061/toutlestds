package calendrier;

import java.util.Scanner;

public class Calendrier {

    static String nomMois(int numMois) {
        String mois[] = {
            " ", "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Decembre"
        };
        return mois[numMois];
    }

    static void afficherTitre(int mois, int année) {
        System.out.println("   ----------------");
        System.out.println("     " + nomMois(mois) + " " + année + "      ");
        System.out.println("   ----------------");
    }

    static void afficherEntête() {
        System.out.println(" Lu Ma Me Je Ve Sa Di");

    }

    static void afficherMois(int décalage, int nombreJours) {
       
        if (décalage ==2){
            décalage = 0;
        }
        if (décalage ==3){
            décalage = 1;
        }
        if (décalage ==4){
            décalage = 2;
        }
        if (décalage ==5){
            décalage = 3;
        }
        if (décalage ==6){
            décalage = 4;
        }
        if (décalage ==1){
            décalage = 6;
        }
        if (décalage ==2){
            décalage = 5;
        }
        
        if ((décalage==0) && (nombreJours==31)){
        System.out.println(" 1"+" "+" 2"+" "+" 3"+" "+" 4"+" "+" 5"+" "+" 6"+" "+" 7");
        System.out.println(" 8"+" "+" 9"+" "+"10"+" "+"11"+" "+"12"+" "+"13"+" "+"14");
        System.out.println("15"+" "+"16"+" "+"17"+" "+"18"+" "+"19"+" "+"20"+" "+"21");
        System.out.println("22"+" "+"23"+" "+"24"+" "+"25"+" "+"26"+" "+"27"+" "+"28");
        System.out.println("29"+" "+"30"+" "+"31");}
        if ((décalage==1) && (nombreJours==31)){
        System.out.println("  "+" "+" 1"+" "+" 2"+" "+" 3"+" "+" 4"+" "+" 5"+" "+" 6");
        System.out.println(" 7"+" "+" 8"+" "+" 9"+" "+"10"+" "+"11"+" "+"12"+" "+"13");
        System.out.println("14"+" "+"15"+" "+"16"+" "+"17"+" "+"18"+" "+"19"+" "+"20");
        System.out.println("21"+" "+"22"+" "+"23"+" "+"24"+" "+"25"+" "+"26"+" "+"27");
        System.out.println("28"+" "+"29"+" "+"30"+" 31");}
        if ((décalage==2) && (nombreJours==31)){
        System.out.println("  "+" "+"  "+" "+" 1"+" "+" 2"+" "+" 3"+" "+" 4"+" "+" 5");
        System.out.println(" 6"+" "+" 7"+" "+" 8"+" "+" 9"+" "+"10"+" "+"11"+" "+"12");
        System.out.println("13"+" "+"14"+" "+"15"+" "+"16"+" "+"17"+" "+"18"+" "+"19");
        System.out.println("20"+" "+"21"+" "+"22"+" "+"23"+" "+"24"+" "+"25"+" "+"26");
        System.out.println("27"+" "+"28"+" 29"+" 30"+" "+"31");}
        if ((décalage==3) && (nombreJours==31)){
        System.out.println("  "+" "+"  "+" "+"  "+" "+" 1"+" "+" 2"+" "+" 3"+" "+" 4");
        System.out.println(" 5"+" "+" 6"+" "+" 7"+" "+" 8"+" "+" 9"+" "+"10"+" "+"11");
        System.out.println("12"+" "+"13"+" "+"14"+" "+"15"+" "+"16"+" "+"17"+" "+"18");
        System.out.println("19"+" "+"20"+" "+"21"+" "+"22"+" "+"23"+" "+"24"+" "+"25");
        System.out.println("26"+" "+"27"+" "+"28"+" 29"+" 30"+" 31");}
        if ((décalage==4) && (nombreJours==31)){
        System.out.println("  "+" "+"  "+" "+"  "+" "+"  "+" "+" 1"+" "+" 2"+" "+" 3");
        System.out.println(" 4"+" "+" 5"+" "+" 6"+" "+" 7"+" "+" 8"+" "+" 9"+" "+"10");
        System.out.println("11"+" "+"12"+" "+"13"+" "+"14"+" "+"15"+" "+"16"+" "+"17");
        System.out.println("18"+" "+"19"+" "+"20"+" "+"21"+" "+"22"+" "+"23"+" "+"24");
        System.out.println("25"+" "+"26"+" "+"27"+" 28"+" 29"+" 30"+" 31");}
        if ((décalage==5) && (nombreJours==31)){
        System.out.println("  "+" "+"  "+" "+"  "+" "+"  "+" "+"  "+" "+" 1"+" "+" 2");
        System.out.println(" 3"+" "+" 4"+" "+" 5"+" "+" 6"+" "+" 7"+" "+" 8"+" "+" 9");
        System.out.println("10"+" "+"11"+" "+"12"+" "+"13"+" "+"14"+" "+"15"+" "+"16");
        System.out.println("17"+" "+"18"+" "+"19"+" "+"20"+" "+"21"+" "+"22"+" "+"23");
        System.out.println("24"+" "+"25"+" "+"26"+" 27"+" 28"+" 29"+" 30");//5 décalage
        System.out.println("31");}
        if ((décalage==6) && (nombreJours==31)){
        System.out.println("  "+" "+"  "+" "+"  "+" "+"  "+" "+"  "+" "+"  "+" "+" 1");
        System.out.println(" 2"+" "+" 3"+" "+" 4"+" "+" 5"+" "+" 6"+" "+" 7"+" "+" 8");
        System.out.println(" 9"+" "+"10"+" "+"11"+" "+"12"+" "+"13"+" "+"14"+" "+"15");
        System.out.println("16"+" "+"17"+" "+"18"+" "+"19"+" "+"20"+" "+"21"+" "+"22");
        System.out.println("23"+" "+"24"+" "+"25"+" 26"+" 27"+" 28"+" 29");
        System.out.println("30"+" 31");}
        if ((décalage==0) && (nombreJours==30)){
        System.out.println(" 1"+" "+" 2"+" "+" 3"+" "+" 4"+" "+" 5"+" "+" 6"+" "+" 7");
        System.out.println(" 8"+" "+" 9"+" "+"10"+" "+"11"+" "+"12"+" "+"13"+" "+"14");
        System.out.println("15"+" "+"16"+" "+"17"+" "+"18"+" "+"19"+" "+"20"+" "+"21");
        System.out.println("22"+" "+"23"+" "+"24"+" "+"25"+" "+"26"+" "+"27"+" "+"28");
        System.out.println("29"+" "+"30");}
        if ((décalage==1) && (nombreJours==30)){
        System.out.println("  "+" "+" 1"+" "+" 2"+" "+" 3"+" "+" 4"+" "+" 5"+" "+" 6");
        System.out.println(" 7"+" "+" 8"+" "+" 9"+" "+"10"+" "+"11"+" "+"12"+" "+"13");
        System.out.println("14"+" "+"15"+" "+"16"+" "+"17"+" "+"18"+" "+"19"+" "+"20");
        System.out.println("21"+" "+"22"+" "+"23"+" "+"24"+" "+"25"+" "+"26"+" "+"27");
        System.out.println("28"+" "+"29"+" "+"30");}
        if ((décalage==2) && (nombreJours==30)){
        System.out.println("  "+" "+"  "+" "+" 1"+" "+" 2"+" "+" 3"+" "+" 4"+" "+" 5");
        System.out.println(" 6"+" "+" 7"+" "+" 8"+" "+" 9"+" "+"10"+" "+"11"+" "+"12");
        System.out.println("13"+" "+"14"+" "+"15"+" "+"16"+" "+"17"+" "+"18"+" "+"19");
        System.out.println("20"+" "+"21"+" "+"22"+" "+"23"+" "+"24"+" "+"25"+" "+"26");
        System.out.println("27"+" "+"28"+" 29"+" 30");}
        if ((décalage==3) && (nombreJours==30)){
        System.out.println("  "+" "+"  "+" "+"  "+" "+" 1"+" "+" 2"+" "+" 3"+" "+" 4");
        System.out.println(" 5"+" "+" 6"+" "+" 7"+" "+" 8"+" "+" 9"+" "+"10"+" "+"11");
        System.out.println("12"+" "+"13"+" "+"14"+" "+"15"+" "+"16"+" "+"17"+" "+"18");
        System.out.println("19"+" "+"20"+" "+"21"+" "+"22"+" "+"23"+" "+"24"+" "+"25");
        System.out.println("26"+" "+"27"+" "+"28"+" 29"+" 30");}
        if ((décalage==4) && (nombreJours==30)){
        System.out.println("  "+" "+"  "+" "+"  "+" "+"  "+" "+" 1"+" "+" 2"+" "+" 3");
        System.out.println(" 4"+" "+" 5"+" "+" 6"+" "+" 7"+" "+" 8"+" "+" 9"+" "+"10");
        System.out.println("11"+" "+"12"+" "+"13"+" "+"14"+" "+"15"+" "+"16"+" "+"17");
        System.out.println("18"+" "+"19"+" "+"20"+" "+"21"+" "+"22"+" "+"23"+" "+"24");
        System.out.println("25"+" "+"26"+" "+"27"+" 28"+" 29"+" 30");}
        if ((décalage==5) && (nombreJours==30)){
        System.out.println("  "+" "+"  "+" "+"  "+" "+"  "+" "+"  "+" "+" 1"+" "+" 2");
        System.out.println(" 3"+" "+" 4"+" "+" 5"+" "+" 6"+" "+" 7"+" "+" 8"+" "+" 9");
        System.out.println("10"+" "+"11"+" "+"12"+" "+"13"+" "+"14"+" "+"15"+" "+"16");
        System.out.println("17"+" "+"18"+" "+"19"+" "+"20"+" "+"21"+" "+"22"+" "+"23");
        System.out.println("24"+" "+"25"+" "+"26"+" 27"+" 28"+" 29"+" 30");}
        if ((décalage==6) && (nombreJours==30)){
        System.out.println("  "+" "+"  "+" "+"  "+" "+"  "+" "+"  "+" "+"  "+" "+" 1");
        System.out.println(" 2"+" "+" 3"+" "+" 4"+" "+" 5"+" "+" 6"+" "+" 7"+" "+" 8");
        System.out.println(" 9"+" "+"10"+" "+"11"+" "+"12"+" "+"13"+" "+"14"+" "+"15");
        System.out.println("16"+" "+"17"+" "+"18"+" "+"19"+" "+"20"+" "+"21"+" "+"22");
        System.out.println("23"+" "+"24"+" "+"25"+" 26"+" 27"+" 28"+" 29");
        System.out.println("30");}
        if ((décalage==0) && (nombreJours==29)){
        System.out.println(" 1"+" "+" 2"+" "+" 3"+" "+" 4"+" "+" 5"+" "+" 6"+" "+" 7");
        System.out.println(" 8"+" "+" 9"+" "+"10"+" "+"11"+" "+"12"+" "+"13"+" "+"14");
        System.out.println("15"+" "+"16"+" "+"17"+" "+"18"+" "+"19"+" "+"20"+" "+"21");
        System.out.println("22"+" "+"23"+" "+"24"+" "+"25"+" "+"26"+" "+"27"+" "+"28");
        System.out.println("29");}
        if ((décalage==1) && (nombreJours==29)){
        System.out.println("  "+" "+" 1"+" "+" 2"+" "+" 3"+" "+" 4"+" "+" 5"+" "+" 6");
        System.out.println(" 7"+" "+" 8"+" "+" 9"+" "+"10"+" "+"11"+" "+"12"+" "+"13");
        System.out.println("14"+" "+"15"+" "+"16"+" "+"17"+" "+"18"+" "+"19"+" "+"20");
        System.out.println("21"+" "+"22"+" "+"23"+" "+"24"+" "+"25"+" "+"26"+" "+"27");
        System.out.println("28"+" "+"29");}
        if ((décalage==2) && (nombreJours==29)){
        System.out.println("  "+" "+"  "+" "+" 1"+" "+" 2"+" "+" 3"+" "+" 4"+" "+" 5");
        System.out.println(" 6"+" "+" 7"+" "+" 8"+" "+" 9"+" "+"10"+" "+"11"+" "+"12");
        System.out.println("13"+" "+"14"+" "+"15"+" "+"16"+" "+"17"+" "+"18"+" "+"19");
        System.out.println("20"+" "+"21"+" "+"22"+" "+"23"+" "+"24"+" "+"25"+" "+"26");
        System.out.println("27"+" "+"28"+" 29");}
        if ((décalage==3) && (nombreJours==29)){
        System.out.println("  "+" "+"  "+" "+"  "+" "+" 1"+" "+" 2"+" "+" 3"+" "+" 4");
        System.out.println(" 5"+" "+" 6"+" "+" 7"+" "+" 8"+" "+" 9"+" "+"10"+" "+"11");
        System.out.println("12"+" "+"13"+" "+"14"+" "+"15"+" "+"16"+" "+"17"+" "+"18");
        System.out.println("19"+" "+"20"+" "+"21"+" "+"22"+" "+"23"+" "+"24"+" "+"25");
        System.out.println("26"+" "+"27"+" "+"28"+" 29");}
        if ((décalage==4) && (nombreJours==29)){
        System.out.println("  "+" "+"  "+" "+"  "+" "+"  "+" "+" 1"+" "+" 2"+" "+" 3");
        System.out.println(" 4"+" "+" 5"+" "+" 6"+" "+" 7"+" "+" 8"+" "+" 9"+" "+"10");
        System.out.println("11"+" "+"12"+" "+"13"+" "+"14"+" "+"15"+" "+"16"+" "+"17");
        System.out.println("18"+" "+"19"+" "+"20"+" "+"21"+" "+"22"+" "+"23"+" "+"24");
        System.out.println("25"+" "+"26"+" "+"27"+" 28"+" 29");}
        if ((décalage==5) && (nombreJours==29)){
        System.out.println("  "+" "+"  "+" "+"  "+" "+"  "+" "+"  "+" "+" 1"+" "+" 2");
        System.out.println(" 3"+" "+" 4"+" "+" 5"+" "+" 6"+" "+" 7"+" "+" 8"+" "+" 9");
        System.out.println("10"+" "+"11"+" "+"12"+" "+"13"+" "+"14"+" "+"15"+" "+"16");
        System.out.println("17"+" "+"18"+" "+"19"+" "+"20"+" "+"21"+" "+"22"+" "+"23");
        System.out.println("24"+" "+"25"+" "+"26"+" 27"+" 28"+" 29");}
        if ((décalage==6) && (nombreJours==29)){
        System.out.println("  "+" "+"  "+" "+"  "+" "+"  "+" "+"  "+" "+"  "+" "+" 1");
        System.out.println(" 2"+" "+" 3"+" "+" 4"+" "+" 5"+" "+" 6"+" "+" 7"+" "+" 8");
        System.out.println(" 9"+" "+"10"+" "+"11"+" "+"12"+" "+"13"+" "+"14"+" "+"15");
        System.out.println("16"+" "+"17"+" "+"18"+" "+"19"+" "+"20"+" "+"21"+" "+"22");
        System.out.println("23"+" "+"24"+" "+"25"+" 26"+" 27"+" 28"+" 29");}
        if ((décalage==0) && (nombreJours==28)){
        System.out.println(" 1"+" "+" 2"+" "+" 3"+" "+" 4"+" "+" 5"+" "+" 6"+" "+" 7");
        System.out.println(" 8"+" "+" 9"+" "+"10"+" "+"11"+" "+"12"+" "+"13"+" "+"14");
        System.out.println("15"+" "+"16"+" "+"17"+" "+"18"+" "+"19"+" "+"20"+" "+"21");
        System.out.println("22"+" "+"23"+" "+"24"+" "+"25"+" "+"26"+" "+"27"+" "+"28");}
        if ((décalage==1) && (nombreJours==28)){
        System.out.println("  "+" "+" 1"+" "+" 2"+" "+" 3"+" "+" 4"+" "+" 5"+" "+" 6");
        System.out.println(" 7"+" "+" 8"+" "+" 9"+" "+"10"+" "+"11"+" "+"12"+" "+"13");
        System.out.println("14"+" "+"15"+" "+"16"+" "+"17"+" "+"18"+" "+"19"+" "+"20");
        System.out.println("21"+" "+"22"+" "+"23"+" "+"24"+" "+"25"+" "+"26"+" "+"27");
        System.out.println("28");}
        if ((décalage==2) && (nombreJours==28)){
        System.out.println("  "+" "+"  "+" "+" 1"+" "+" 2"+" "+" 3"+" "+" 4"+" "+" 5");
        System.out.println(" 6"+" "+" 7"+" "+" 8"+" "+" 9"+" "+"10"+" "+"11"+" "+"12");
        System.out.println("13"+" "+"14"+" "+"15"+" "+"16"+" "+"17"+" "+"18"+" "+"19");
        System.out.println("20"+" "+"21"+" "+"22"+" "+"23"+" "+"24"+" "+"25"+" "+"26");
        System.out.println("27"+" "+"28");}
        if ((décalage==3) && (nombreJours==28)){
        System.out.println("  "+" "+"  "+" "+"  "+" "+" 1"+" "+" 2"+" "+" 3"+" "+" 4");
        System.out.println(" 5"+" "+" 6"+" "+" 7"+" "+" 8"+" "+" 9"+" "+"10"+" "+"11");
        System.out.println("12"+" "+"13"+" "+"14"+" "+"15"+" "+"16"+" "+"17"+" "+"18");
        System.out.println("19"+" "+"20"+" "+"21"+" "+"22"+" "+"23"+" "+"24"+" "+"25");
        System.out.println("26"+" "+"27"+" "+"28");}
        if ((décalage==4) && (nombreJours==28)){
        System.out.println("  "+" "+"  "+" "+"  "+" "+"  "+" "+" 1"+" "+" 2"+" "+" 3");
        System.out.println(" 4"+" "+" 5"+" "+" 6"+" "+" 7"+" "+" 8"+" "+" 9"+" "+"10");
        System.out.println("11"+" "+"12"+" "+"13"+" "+"14"+" "+"15"+" "+"16"+" "+"17");
        System.out.println("18"+" "+"19"+" "+"20"+" "+"21"+" "+"22"+" "+"23"+" "+"24");
        System.out.println("25"+" "+"26"+" "+"27"+" 28");}
        if ((décalage==5) && (nombreJours==28)){
        System.out.println("  "+" "+"  "+" "+"  "+" "+"  "+" "+"  "+" "+" 1"+" "+" 2");
        System.out.println(" 3"+" "+" 4"+" "+" 5"+" "+" 6"+" "+" 7"+" "+" 8"+" "+" 9");
        System.out.println("10"+" "+"11"+" "+"12"+" "+"13"+" "+"14"+" "+"15"+" "+"16");
        System.out.println("17"+" "+"18"+" "+"19"+" "+"20"+" "+"21"+" "+"22"+" "+"23");
        System.out.println("24"+" "+"25"+" "+"26"+" 27"+" 28");}
        if ((décalage==6) && (nombreJours==28)){
        System.out.println("  "+" "+"  "+" "+"  "+" "+"  "+" "+"  "+" "+"  "+" "+" 1");
        System.out.println(" 2"+" "+" 3"+" "+" 4"+" "+" 5"+" "+" 6"+" "+" 7"+" "+" 8");
        System.out.println(" 9"+" "+"10"+" "+"11"+" "+"12"+" "+"13"+" "+"14"+" "+"15");
        System.out.println("16"+" "+"17"+" "+"18"+" "+"19"+" "+"20"+" "+"21"+" "+"22");
        System.out.println("23"+" "+"24"+" "+"25"+" 26"+" 27"+" 28");}
    }

    static boolean estBissextile(int année) {

        return ((année % 100 != 0 && année % 4 == 0) || année % 400 == 0);
    }

    static int nombreJours(int mois, int année) {
        int nbJours;
        nbJours = 0;

        if (estBissextile(année) == true && (mois == 2)) {
            nbJours = 29;
        }
        if (estBissextile(année) == false && (mois == 2)) {
            nbJours = 28;
        }
        if ((mois == 4) || (mois == 6) || (mois == 9) || (mois == 11) && (mois != 2)) {
            nbJours = 30;
        }
        if ((mois == 1) || (mois == 3) || (mois == 5) || (mois == 7) || (mois == 8) || (mois == 10) || (mois == 12))  {
            nbJours = 31;
        }
        return nbJours;

    }

    static int numéroJour(int jour, int mois, int année) {
        int m = mois;
        if (m == 1) {
            m = 13;
            année--;

        }
        if (m == 2) {
            m = 14;
            année--;

        }
        int q = jour;
        int j = année / 100;
        int k = année % 100;

        int h = (q + ((13 * (m + 1)) / 5) + k + (k / 4) + (j / 4) + (5 * j)) % 7;
        return h;
    }

    public static void main(String[] args) {
       
        //Mois

        System.out.println("Tapez le numéro du mois");
        Scanner clavier = new Scanner(System.in);
        int mois = clavier.nextInt();

        //Année
        
        System.out.println("Tapez l'année");
        Scanner clavier2 = new Scanner(System.in);
        int année = clavier2.nextInt();
        
        //Main
        
        int nombreJours = nombreJours(mois, année);
        afficherTitre(mois, année);
        afficherEntête();
        int décalage = numéroJour(1, mois,année);
        afficherMois(décalage, nombreJours);
        

    }

}
