package Dev2TD2;

public class Moment {

    private int hour;
    private int minute;
    private int seconde;

    public Moment() {
        this.hour = 0;
        this.minute = 0;
        this.seconde = 0;
    }

    public Moment(int hour, int minute, int seconde) {

        if (hour > 23 && hour < 0) {
            throw new IllegalArgumentException();
        }
        if (minute > 59 && minute < 0) {
            throw new IllegalArgumentException();
        }
        if (seconde > 59 && seconde < 0) {
            throw new IllegalArgumentException();
        }

        this.hour = hour;
        this.minute = minute;
        this.seconde = seconde;
    }

    public boolean equals(Moment other) {
        boolean equals;
        if (getClass() != other.getClass()) {
            return equals = false;
        } else {
            return equals = true;
        }
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public int getSeconde() {
        return seconde;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public void setSeconde(int seconde) {
        this.seconde = seconde;
    }

    @Override
    public String toString() {

        return getHour() + " : " + getMinute() + " : " + getSeconde();
    }

    public int toSeconde() {
        return getHour()*3600 + getMinute()*60 + getSeconde();

    }

    public int compareTo(Moment other) {
        int diff = 0;
        if (other.toSeconde() != toSeconde()) {
            diff = toSeconde() - other.toSeconde();
            return diff;
        } else {
            return 0;
        }

    }

}
