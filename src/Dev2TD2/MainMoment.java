package Dev2TD2;

public class MainMoment {

    public static void main(String[] args) {

        Moment moment1 = new Moment(8, 14, 59);
        Moment moment2 = new Moment(8, 15, 0);
        Moment moment3 = moment2;
        Moment moment4 = new Moment(10, 15, 0);
        Moment moment0 = new Moment();
        System.out.println(moment1 + " == " + moment2 + " : " + (moment1 == moment2));
        System.out.println(moment1 + " == " + moment3 + " : " + (moment1 == moment3));
        System.out.println(moment1 + " == " + moment4 + " : " + (moment1 == moment4));
        System.out.println(moment1 + " equals " + moment2 + " : " + moment1.equals(moment2));
        System.out.println(moment1 + " equals " + moment3 + " : " + moment1.equals(moment3));
        System.out.println(moment1 + " equals " + moment4 + " : " + moment1.equals(moment4));
        System.out.println(moment1.getHour() + " : " + moment1.getMinute() + " : " + moment1.getSeconde());
        System.out.println(moment1);
        System.out.println(moment3);
        
        System.out.println(moment1.compareTo(moment3));
    }
}
