/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exotd5;

import java.util.Scanner;

public class ExoTD5Suite {

    @SuppressWarnings("empty-statement")
    public static void main(String[] args) {
        Scanner clavier = new Scanner(System.in);
        String texte = clavier.nextLine(); //lit du texte au clavier

        System.out.println(texte);
        System.out.println("Le texte entré est: " + texte);

        int longueur = texte.length(); //le nombre de caractères
        System.out.println("La longueur du texte est: " + longueur);

        char premièreLettre = texte.charAt(0);
        char dernièreLettre = texte.charAt(longueur - 1);
        System.out.println(premièreLettre);
        System.out.println(dernièreLettre);
        boolean check;
        check = false;
        String voyelles;
        voyelles = "aeiouy";

        for (int u = 0; u < voyelles.length(); u++) {
            if (voyelles.charAt(u) == texte.charAt(0)) {
                check = true;
                System.out.println("C'est une voyelle");
            }
        }
        if (check == false) {
            System.out.println("C'est une consonne");
        }

        for (int i = 0; i < texte.length(); i = i + 1) {
            for (int u = 0; u < voyelles.length(); u++) {
                if (voyelles.charAt(u) == texte.charAt(i)) {
                    check = true;
                    System.out.print(texte.charAt(i));
                }
            }
        }
        System.out.println(); //passe à la ligne

        for (int v = texte.length() - 1; v >= 0; v = v - 1) { // parcours la chaine de caractères à l’envers
            System.out.print(texte.charAt(v));
        }

    }
}
