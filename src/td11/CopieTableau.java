package td11;
import java.util.Arrays;
public class CopieTableau {
    public static void main(String[] args) {
        int[] tableau = {1, 2, 3, 4, 5};
        afficherTab(tableau);
        int[] copie1 = copie(tableau);
        int[] miroir = miroir(tableau);
        tableau[0] = 9;
        System.out.print("tableau: ");
        afficherTab(tableau);
        System.out.print("copie1: ");
        afficherTab(copie1);
        System.out.print("miroir : ");
        afficherTab(miroir);
    }
    static int[] copie(int[] tab) {
        int[] copie = new int[tab.length];
        for (int i = 0; i < tab.length; i++) {
            copie[i] = tab[i];
        }
        return copie;
    }
    static void afficherTab(int[] tab) {
        for (int element : tab) {
            System.out.print(element + " ");
        }
        System.out.println();
    }
    static int[] miroir(int[] tab) {
        int[] miroir = new int[tab.length];
        for (int i = 0; i <= tab.length - 1; i++) {
            miroir[i] = tab[(tab.length - i) - 1];
        }
        System.out.println(Arrays.toString(miroir));
        return miroir;
    }
}
