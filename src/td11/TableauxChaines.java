package td11;

public class TableauxChaines {

    public static void main(String[] args) {
        String[] mots = {"the", "quick", "brown", "fox", "jumps", "over", "the",
            "lazy", "dog"};
        for (String patate : mots) { // pour chaque mot dans le tableau ’mots’
            System.out.print(patate + " "); // affiche le mot suivi d’un espace.
        }
        System.out.println(); // passe à la ligne.
        afficheTableau(mots);
        afficherTailles(mots);// la même chose mais dans une méthode.
        plusLongMot(mots);
    }

    static void afficheTableau(String[] mots) {
        for (String mot : mots) {
            System.out.print(mot + " ");
        }
        System.out.println();
    }
//affiche la taille de chaque String d’un tableau.

    static void afficherTailles(String[] mots) {
        for (int i = 0; i < mots.length; i++) {
            System.out.print(mots[i].length());

        }
        System.out.println();
    }
//affiche le plus long mot.

    static void plusLongMot(String[] mots) {
        int lengthMot = 0;
        for (int i = 1; i < mots.length; i++) {
            int lengthMot2 = mots[i].length();
            if (lengthMot <= lengthMot2) {
                lengthMot = lengthMot2;
                System.out.print(mots[i] + " ");
            }
        }
        // tout les mots du tableau dont la taille est égale à lengthMot

    }
}
