package td11;

public class TD11 {

    public static void main(String[] args) {
        int[] tab = {1, 2, 3, 4, 5};
        System.out.println("taille: " + tab.length);
        System.out.println("1er élément: " + tab[0]);
        System.out.println("dernier élément: " + tab[tab.length - 1]);
        System.out.println("autre élément: " + tab[2]);
        System.out.println("affiche un drôle de truc: " + tab);

        afficherTab(tab);
        incrémenter(tab);
        afficherTab(tab);

        int[] tab2 = new int[10];
        afficherTab(tab2);
    }

    static void multiplier(int[] tab, int valeur){
        for (int values : tab) {
            values = values * valeur;
        }
    }

    static void afficherTab(int[] tab) {
        for (int valeur : tab) { // pour chaque valeur dans le tableau ’tab’
            System.out.print(valeur + " ");
        }
        System.out.println(); // on passe à la ligne
    }

    static void incrémenter(int[] tab) {
        for (int i = 0; i < tab.length; i++) {
            tab[i]++;
        }
    }
}
