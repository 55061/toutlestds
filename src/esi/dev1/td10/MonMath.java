/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esi.dev1.td10;

/**
 *
 * @author Vincs
 */
public class MonMath {

    static double abs(double x) {
        double solution = x;

        if (x < 0) {
            solution = -x;
        }

        return solution;
    }

    public static void main(String[] args) {
        int entrée;
        int sortie;

        entrée = 4;
        sortie = 4;
        System.out.println("Teste 1 : abs(" + entrée + ") = " + sortie + " ? "
                + (abs(entrée) == sortie));

        entrée = -6;
        sortie = 6;
        System.out.println("Teste 2 : abs(" + entrée + ") = " + sortie + " ? "
                + (abs(entrée) == sortie));
    }

}
