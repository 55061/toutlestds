
package tdméthode;
import java.util.Scanner;


public class Cercle {
    static double périmètre(double rayon){
        if (rayon < 0){
            throw new IllegalArgumentException("Le rayon doit être positif: "+rayon);
        }
        return 2 * Math.PI * rayon;
    
    }
    
    public static void main(String[] args) {
        Scanner clavier = new Scanner(System.in);
        int rayon = clavier.nextInt();
    
        System.out.println("Le périmetre du cercle de rayon est :" + périmètre(rayon));
       
    }
    

}
